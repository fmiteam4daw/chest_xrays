import torchvision

from models.network_v1 import NetworkV1
from models.network_v1_cam import NetworkV1Cam

def construct_model(config, num_classes):
    if config['arch'] == 'resnext50':
        base = torchvision.models.resnext50_32x4d(pretrained=True)
    elif config['arch'] == 'resnet34':
        base = torchvision.models.resnet34(pretrained=True)
    elif config['arch'] == 'mobilenetv2':
        base = torchvision.models.mobilenet_v2(pretrained=True)
    elif config['arch'] == 'vgg19':
        base = torchvision.models.vgg19(pretrained=True)
    elif config['arch'] == 'densenet121':
        base = torchvision.models.densenet121(pretrained=True)
    else:
        print("Invalid model name, exiting...")
        exit()

    if config['version'] == '1':
        model = NetworkV1(base, num_classes)
    elif config['version'] == '1CAM':
        model = NetworkV1Cam(base, num_classes)

    return model