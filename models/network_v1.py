import torch.nn as nn


class NetworkV1(nn.Module):
    def __init__(self, base, num_classes):
        super().__init__()
        self.base = base

        if hasattr(base, 'fc'):
            in_features = self.base.fc.in_features
            self.base.fc = nn.Linear(in_features, num_classes)
        elif isinstance(base.classifier, nn.Linear): # densenet121
            in_features = self.base.classifier.in_features
            self.base.classifier = nn.Sequential(
                                        nn.Linear(in_features, num_classes),
                                        nn.Sigmoid())
        else: # mobilenetv2 / vgg19
            in_features = self.base.classifier[-1].in_features
            self.base.classifier[-1] = nn.Linear(in_features=in_features, out_features=num_classes, bias=True)

    def forward(self, x):
        fc = self.base(x)
        
        return fc